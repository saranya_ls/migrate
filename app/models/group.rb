class Group < ApplicationRecord
	
	GRP_TYPE=["closed","public","secret"]
	validates :name, presence:true, length:{maximum:30}
	validates :group_type, inclusion:{in:GRP_TYPE}, presence: true
    validates :user_id, presence:true

	has_many :goup_members

	belongs_to :owner, :class_name=>'User', :foreign_key => 'user_id'
	has_many :invites
end


