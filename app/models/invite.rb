class Invite < ApplicationRecord

belongs_to :group
belongs_to :invitedby, class_name: 'User', foreign_key: 'user_id'
belongs_to :invitedto,class_name: 'User', foreign_key: 'user_id'

validates :group_id, presence: true
validates :invitedby_id, presence: true
validates :invitedto_id, presence: true

end
