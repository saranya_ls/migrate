class User < ApplicationRecord

	EMAIL_EXP=/\A[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\Z/i
	has_secure_password
	validates :f_name, presence: true
	validates :s_name, presence: true
	validates :email, presence: true, format:EMAIL_EXP, length: {maximum:30}, uniqueness:true
   	validates :password, length: {in: 6..12}
	
	validates :dob, presence: true


    has_many  :groups 
    has_many  :invites
end
