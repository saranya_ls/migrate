class GroupMember < ApplicationRecord
	
	belongs_to :group
	belongs_to :user

	validates :group_id, presence: true
	validates_uniqueness_of :members, scope: :user_id
end
