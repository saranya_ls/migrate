class CreateGroupMembers < ActiveRecord::Migration[5.2]
  def change
    create_table :group_members do |t|
       t.integer :user_id, null:false
       t.integer :group_id 
       
       t.references :group, foreign_key: true, null:false 
       t.timestamps
    end
     
   
  end
end
