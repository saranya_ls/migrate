class ChangeColumnNameInGroup < ActiveRecord::Migration[5.2]
  def change
  	rename_column :groups, :user_id, :owner_id
  end
end
