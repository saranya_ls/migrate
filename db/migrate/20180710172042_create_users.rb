class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :f_name, null: false
      t.string :s_name, null: false
      t.string :email, null: false
      t.string :password, null: false
      t.date :dob, null: false

      t.timestamps
end
      add_index :users, :email, unique: true
  
  end
end
