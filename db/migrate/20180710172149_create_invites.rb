class CreateInvites < ActiveRecord::Migration[5.2]
  def change
    create_table :invites do |t|
      t.integer :invitedby_id, null:false
      t.integer  :invitedto_id , null:false   
      t.boolean :status, null: false
      t.references :group, foreign_key: true, unique:true, null: false
      t.timestamp :time, null: false
        t.timestamps
    end

  end
end
