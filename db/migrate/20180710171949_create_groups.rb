class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|
      t.integer :user_id
      t.string :name, null: false
      t.string :type, null: false
      t.string :description, null: false
      t.references :user, index: true, foreign_key:true, unique:true, null:false
      t.timestamps


    end


  end
end
